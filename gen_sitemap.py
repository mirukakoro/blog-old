import os

def get_directory_structure(rootdir = os.getcwd()):
    '''
    Get structure of directory from filesystem, as rootdir as root.
    '''
    dir_    = {}
    rootdir = rootdir.rstrip(os.sep)
    start   = rootdir.rfind(os.sep) + 1
    for path, dirs, files in os.walk(rootdir):
        folders = path[start:].split(os.sep)
        subdir  = dict.fromkeys(files)
        parent  = reduce(dict.get, folders[:-1], dir_)
        parent[folders[-1]] = subdir
    return dir_

def get_sitemap(
        sitemap_path = './templates/content/sitemap.html', 
        sitemap_key  = '<!--SITEMAP-->', 
        write = True, 
        nest_fmt = '<ul class="tree" id="tree"><li><span class="caret">{name}</span><ul class="nested">{content}</ul></li></ul>', 
        term_fmt = '<li><a href="{url}">{name}</a></li>', 
        rootdir = './content', 
    ):
    print('get_sitemap', 'start')
    source = ''
    with open(sitemap_path, 'r', ) as file:
        source = file.read()
    result = '<!-- Sitemap automatically enerated by gen_sitemap.py by Colourdelete on GitLab.com. -->'
    dir_struct = get_directory_structure(rootdir, )
    def render_dir_struct(dir_struct, nest_fmt, term_fmt, keys, ):
        result = ''
        for key in list(dir_struct.keys()):
            value = dir_struct[key]
            print('render_dir_struct', key, value)
            if value == None:
                result += term_fmt.format(name=key, url='{{ url_for(\'pages\', page=\'<path>\') }}'.replace('<path>', os.path.join(*keys)))
            else:
                result += nest_fmt.format(name=key, content=render_dir_struct(value, nest_fmt, term_fmt, keys + [key], ))
        return result
    result += render_dir_struct(dir_struct, nest_fmt, term_fmt, [])
    print('get_sitemap', result)
    source.replace(sitemap_key, result, )
    if write:
        with open(sitemap_path, 'w', ) as file:
            file.write(source, )
    print('get_sitemap', 'done')
    return source

if __name__ == '__main__':
    print('gen_sitemap.py by Colourdelete on GitLab.com.')
    print('Generating sitemap with default params...')
    get_sitemap()
    print('Done.')
