from pathlib import Path
import os
from flask import Flask, render_template, abort, send_file
from flask_frozen import Freezer
import jinja2
from os import listdir
from os.path import isfile, join

app = Flask(__name__)
app.config['FREEZER_BASE_URL'] = 'https://colourdelete.gitlab.io/blog'
app.config['FREEZER_DESTINATION'] = 'public'
freezer = Freezer(app)


@app.cli.command()
def freeze():
    freezer.freeze()

@app.cli.command()
def serve():
    freezer.run()

@freezer.register_generator
def page_generator():
    """
    Frozen-Flask doesn't know what to generate when a route contains a
    variable. This function resolves this, refer to Frozen-Flask's
    documentation for more information.
    """
    for template_path in app.jinja_env.list_templates():
        try:
            page = Path(template_path).relative_to("content").stem
            yield 'pages', {'page': page}
        except ValueError:
            pass

@app.route('/')
def index():
    return pages('index')

@app.route('/sitemap')
def sitemap():
    return render_template('content/sitemap.html')

def get_past_files(path='./past_files.txt'):
    with open(path, 'r') as file:
        past_files = file.read().split('\n')
    return past_files

past_files = get_past_files()

@app.route('/<path:page>')
def pages(page):
    searchpath = 'templates/content/'
    files = [f for f in listdir(searchpath) if isfile(join(searchpath, f))]
    for file_ in files:
        print(page, file_, end=' ')
        if page in file_:
            print('match', file_.split('.')[-1])
            if file_.split('.')[-1] in ('html', 'css', 'js', 'txt', ):
                return send_file('templates/content/' + file_)
            elif file_.split(' ')[-1] == 'renderthis':
                return render_template('content/' + file_)
            elif file_.split(' ')[-1] == 'sendfilethis':
                return send_file('templates/content/' + file_)
            else:
                try:
                    return render_template('content/' + file_)
                except Exception as err:
                    return send_file('templates/content/' + file_)
        else:
            print('nomatch')
    abort(404)
